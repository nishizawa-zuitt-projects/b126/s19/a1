//Arrow Function
// ES 6 ECMAScript 6

// Function Declaration
function sum(a,b){
	return a+b
}


// Step 1
// let sum2

// Step 2
// let sum2 = () =>

// Step 3

//Expression
let x=9

//Function Expression
let sum2 = (a,b) => a+b // return and {} can be removed if carrying out one task

function isPositive(a){
	return a >= 0
}

let isPositive2 = a => a >= 0

function someNumber(){
	return Math.random()
}

let someNumber2 = () => Math.random()

let hello = () => "Hello World!"

let square = a => a*a

function giveAnswer(num1, num2){
	let total = num1 + num2
	return
}

let giveAnswer2 = (num1, num2) =>{
	let total = num1 + num2
	return total
}

//Object Desctructuring


const address={
	street: "Times St.",
	city: "New York",
	country: "USA",
}

// const street = address.street;
// const city = address.city;
// const country = address.country;

const {city, street, country} = address

//Template Literals (` ${}  `)

let name = "Jino"
let job = "Instructor"

console.log("Hi! My name is " + name + "I'm your " + job)
console.log(`Hi! My name is ${name}.`)

//Array Map Method

const colors=['red','green','blue']

// const items = colors.map(function(color){
// 	return "<li>" + color + "</li>";
// })


const items = colors.map(color => `<li> + ${color} + </li>`)

